# ![image](uploads/bf30bed71f782d01334b73caab75a61a/image.png)

The device categories listed below have OpenRGB support and the specific devices listed have been tested to work with OpenRGB.  If your specific device is not listed, that does not necessarily mean it won't work with OpenRGB, just that it hasn't been tested before.  If you have a device that works with OpenRGB and isn't listed below, please let me know!  I will add it to the list.

If you have a project or product that is compatible with OpenRGB, you may use the above OpenRGB Compatible badge on your project/product page.

# Mode Support

The following support tables use the following symbols to indicate levels of support for the given device.

| Symbol             | Device supports function | OpenRGB supports function |
| ------------------ | ------------------------ | ------------------------- |
| :white_check_mark: | YES                      | YES                       |
| :warning:          | YES                      | NO/PARTIAL                |
| :x:                | NO                       | NO                        |

# Supported Device List

## Motherboard RGB Systems

| Device                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| -------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| [ASUS Aura (SMBus Variants)](ASUS-Aura-Overview)         | * ASUS PRIME X370-Pro<br>* ASUS PRIME X470-Pro<br>* ASUS PRIME X399-A<br>* ASUS PRIME B450M-Gaming<br>* ASUS PRIME Z270-A<br>* ASUS PRIME Z370-A<br>* ASUS ROG Crosshair VI Hero<br>* ASUS ROG STRIX X399-E Gaming<br>* ASUS ROG Strix B350-F Gaming<br>* ASUS ROG Strix B450-F Gaming<br>* ASUS ROG Strix Z270-E<br>* ASUS ROG Strix Z370-E<br>* ASUS ROG Strix Z490-E Gaming<br>* ASUS TUF B450 Plus Gaming | :white_check_mark: | :white_check_mark: |
| [ASUS Aura (USB Variants)](ASUS Aura USB)                | * [ASUS Aura Addressable Headers](ASUS-Aura-Addressable-Header)<br>* ASUS X570 Motherboards                                                                                                                                                                                                                                                                                                                   | :white_check_mark: | :white_check_mark: |
| [Gigabyte Aorus RGB Fusion 1.0](Gigabyte-RGB-Fusion-1.0) | * Gigabyte Aorus X370 Gaming 5                                                                                                                                                                                                                                                                                                                                                                                | :white_check_mark: | :white_check_mark: |
| Gigabyte Aorus RGB Fusion 2.0 (SMBus)                    |                                                                                                                                                                                                                                                                                                                                                                                                               | :warning:          | :white_check_mark: |
| Gigabyte Aorus RGB Fusion 2.0 (USB)                      | * Gigabyte X570 Aorus Extreme<br>* Gigabyte X570 Aorus Master<br>* Gigabyte X570 Aorus Pro<br>* Gigabyte X570 Gaming X<br>* Gigabyte X570 I Aorus Pro Wifi<br>* Gigabyte TRX40 Aorus Master<br>* Gigabyte Z390 Aorus Ultra                                                                                                                                                                                    | :white_check_mark: | :white_check_mark: |
| [ASRock Polychrome RGB](ASRock-Polychrome-RGB)           | * ASRock B450 Steel Legend<br>* ASRock B450M Steel Legend<br>* ASRock Fatal1ty B350 Gaming-ITX/ac<br>* ASRock B450M/ac<br>* ASRock X570 Taichi                                                                                                                                                                                                                                                                | :x:                | :white_check_mark: |
| [MSI-RGB](MSI-RGB)                                       |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :warning:          |

* [ASUS Aura (SMBus Variants)](ASUS-Aura-Overview)
  * ASUS PRIME X370-Pro
  * ASUS PRIME X470-Pro
  * ASUS PRIME X399-A
  * ASUS PRIME B450M-Gaming
  * ASUS PRIME Z270-A
  * ASUS PRIME Z370-A
  * ASUS ROG Crosshair VI Hero
  * ASUS ROG STRIX X399-E Gaming
  * ASUS ROG Strix B350-F Gaming
  * ASUS ROG Strix B450-F Gaming
  * ASUS ROG Strix Z270-E
  * ASUS ROG Strix Z370-E
  * ASUS ROG Strix Z490-F
  * ASUS ROG Strix Z490-E Gaming
  * ASUS TUF B450 Plus Gaming
* [ASUS Aura (USB Variants)](ASUS Aura USB)
  * [ASUS Aura Addressable Headers](ASUS-Aura-Addressable-Header)
  * ASUS X570 Motherboards
* [Gigabyte Aorus RGB Fusion 1.0](Gigabyte-RGB-Fusion-1.0)
  * Gigabyte Aorus X370 Gaming 5
* Gigabyte Aorus RGB Fusion 2.0 (SMBus)
* Gigabyte Aorus RGB Fusion 2.0 (USB)
  * Gigabyte X570 Aorus Extreme
  * Gigabyte X570 Aorus Master
  * Gigabyte X570 Aorus Pro
  * Gigabyte X570 Gaming X
  * Gigabyte X570 I Aorus Pro Wifi
  * Gigabyte TRX40 Aorus Master
  * Gigabyte Z390 Aorus Ultra
* [ASRock Polychrome RGB](ASRock-Polychrome-RGB)
  * ASRock B450 Steel Legend
  * ASRock B450M Steel Legend
  * ASRock Fatal1ty B350 Gaming-ITX/ac
  * ASRock B450M/ac
  * ASRock X570 Taichi
* [MSI-RGB](MSI-RGB)

## RGB RAM Modules

| Device                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| -------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| [ASUS Aura Based](ASUS-Aura-Overview)                    | * G.Skill Trident Z RGB<br>* G.Skill Trident Z Neo<br>* G.Skill Trident Z Royal<br>* Geil Super Luce<br>* Team T-Force Delta RGB<br>* OLOy WarHawk RGB<br>* ADATA SPECTRIX RGB<br>* Thermaltake TOUGHRAM RGB                                                                                                                                                                                                  | :white_check_mark: | :white_check_mark: |
| [Corsair Vengeance RGB](Corsair-Vengeance-RGB)           |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [Corsair Vengeance Pro RGB](Corsair-Vengeance-Pro-RGB)   |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [HyperX RGB Memory](HyperX-Predator-RGB)                 | * HyperX Predator RGB<br>* HyperX Fury RGB                                                                                                                                                                                                                                                                                                                                                                    | :white_check_mark: | :white_check_mark: |
| [Patriot Viper RGB](Patriot-Viper-RGB)                   |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [Crucial Ballistix RGB](Crucial-Ballistix-RGB)           |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |

* [ASUS Aura Based](ASUS-Aura-Overview)
  * G.Skill Trident Z RGB
  * G.Skill Trident Z Neo
  * G.Skill Trident Z Royal
  * Geil Super Luce
  * Team T-Force Delta RGB
  * OLOy WarHawk RGB
  * ADATA SPECTRIX RGB
  * Thermaltake TOUGHRAM RGB
* [Corsair Vengeance RGB](Corsair-Vengeance-RGB)
* [Corsair Vengeance Pro RGB](Corsair-Vengeance-Pro-RGB)
* [HyperX RGB Memory](HyperX-Predator-RGB)
  * HyperX Predator RGB
  * HyperX Fury RGB
* [Patriot Viper RGB](Patriot-Viper-RGB)
* [Crucial Ballistix RGB](Crucial-Ballistix-RGB)

## Graphics Cards

| Device                                                    | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| --------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| [ASUS Aura GPUs](Asus-Aura-GPU)                           | * ASUS ROG Strix RX580<br>* ASUS ROG Strix GTX1080Ti                                                                                                                                                                                                                                                                                                                                                          | :white_check_mark: | :white_check_mark: |
| [EVGA V1 GPUs](EVGA-GPU)                                  | * EVGA GeForce GTX 1070 FTW                                                                                                                                                                                                                                                                                                                                                                                   | :white_check_mark: | :white_check_mark: |
| [Galax GPUs](Galax-GPUs)                                  | * Galax KFA2 RTX2070                                                                                                                                                                                                                                                                                                                                                                                          |                    |                    |
| [Gigabyte Aorus RGB Fusion GPUs](Gigabyte-RGB-Fusion-GPU) | * Gigabyte Aorus GTX1080Ti Xtreme Waterforce WB<br>* Gigabyte RTX2070 SUPER GAMING                                                                                                                                                                                                                                                                                                                            | :white_check_mark: | :white_check_mark: |
| [MSI GPUs](MSI-GPU)                                       | * MSI GeForce RTX 2060 Super Gaming X<br>* MSI GeForce RTX 2070 Super Gaming X Trio<br>* MSI GeForce RTX 2080 Gaming X Trio<br>* MSI GeForce RTX 2080 Super Gaming X Trio<br>* MSI GeForce RTX 2080Ti Gaming X Trio<br>* MSI GeForce RTX 2060 Gaming Z 6G<br>* MSI GeForce RTX 2060 Super ARMOR OC<br>* MSI GeForce RTX 2070 ARMOR<br>* MSI GeForce RTX 2080Ti Sea Hawk EK X                                  | :warning:          | :white_check_mark: |
| [Sapphire Nitro Glow V1](Sapphire-Nitro-Glow-V1)          | * Sapphire RX470/RX480 Nitro+<br>* Sapphire RX570/RX580/RX590 Nitro+<br>* RX Vega 56/64 Nitro+                                                                                                                                                                                                                                                                                                                | :x:                | :white_check_mark: |
| Sapphire Nitro Glow V3                                    | * Sapphire RX5500 XT Nitro+<br>* Sapphire RX5700 XT Nitro+<br>* Sapphire RX6800 XT Nitro+<br>* Sapphire RX6900 XT Nitro+                                                                                                                                                                                                                                                                                      | :x:                | :white_check_mark: |

* [ASUS Aura GPUs](Asus-Aura-GPU)
  * ASUS ROG Strix RX580
  * ASUS ROG Strix GTX1080Ti
* [EVGA GPUs](EVGA-GPU)
  * EVGA GeForce GTX 1070 FTW
* [Galax GPUs](Galax-GPUs)
  * Galax KFA2 RTX2070
* [Gigabyte Aorus RGB Fusion GPUs](Gigabyte-RGB-Fusion-GPU)
  * Gigabyte Aorus GTX1080Ti Xtreme Waterforce WB
  * Gigabyte RTX2070 SUPER GAMING
* [MSI GPUs](MSI-GPU)
  * MSI GeForce GTX 1070 Gaming X
  * MSI GeForce GTX 1660 Gaming X 6G
  * MSI GeForce GTX 1660Ti Gaming X 6G
  * MSI GeForce GTX 1660 Super Gaming X 6G
  * MSI GeForce RTX 2060 Gaming Z 6G
  * MSI GeForce RTX 2060 Gaming Z 6G
  * MSI GeForce RTX 2060 Gaming Z 6G
  * MSI GeForce RTX 2060 Super Gaming X
  * MSI GeForce RTX 2060 Super ARMOR OC
  * MSI GeForce RTX 2070 Gaming Z
  * MSI GeForce RTX 2070 Gaming
  * MSI GeForce RTX 2070 ARMOR
  * MSI GeForce RTX 2070 Super Gaming Trio
  * MSI GeForce RTX 2070 Super Gaming X
  * MSI GeForce RTX 2070 Super Gaming X Trio
  * MSI GeForce RTX 2070 Super Gaming Z Trio
  * MSI GeForce RTX 2080 Gaming X Trio
  * MSI GeForce RTX 2080 Sea Hawk EK X
  * MSI GeForce RTX 2080 Super Gaming X Trio
  * MSI GeForce RTX 2080Ti Gaming X Trio
  * MSI GeForce RTX 2080Ti Sea Hawk EK X
  * MSI GeForce RTX 3070 8GB Gaming X Trio
  * MSI GeForce RTX 3080 10GB Gaming X Trio
  * MSI GeForce RTX 3090 24GB Gaming X Trio
* [Sapphire Nitro Glow V1](Sapphire-Nitro-Glow-V1)
  * Sapphire RX580 Nitro+
* [Sapphire Nitro Glow V2](Sapphire-Nitro-Glow-V2)
* [Sapphire Nitro Glow V3](Sapphire-Nitro-Glow-V3)

## LED Strip and Fan Controllers

| Device                                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| ------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| [ASUS ROG Aura Terminal](ASUS-Aura-Addressable-Header)                   |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [NZXT Hue+](NZXT-Hue-Plus)                                               |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [NZXT Hue 2 Devices](NZXT-Hue-2)                                         | * NZXT Hue 2<br>* NZXT Hue 2 Ambient<br>* NZXT Smart Device V2<br>* NZXT RGB & Fan Controller                                                                                                                                                                                                                                                                                                                 | :white_check_mark: | :white_check_mark: |
| [Corsair Lighting Node Devices](Corsair-Lighting-Node-Devices)           | * Corsair Lighting Node Core<br>* Corsair Lighting Node Pro<br>* Corsair Commander Pro<br>* Corsair LS100 Lighting Kit<br>* Corsair 1000D Obsidian<br>* Corsair SPEC OMEGA RGB<br>* Corsair LT100<br>* [Corsair Lighting Protocol (Arduino)](https://github.com/Legion2/CorsairLightingProtocol)                                                                                                              | :white_check_mark: | :white_check_mark: |
| [Serial LED strips](Serial-LED-Strips)                                   | * Keyboard Visualizer Arduino Protocol<br>* Adalight<br>* TPM2                                                                                                                                                                                                                                                                                                                                                | :white_check_mark: | :x:                |
| [E1.31 Streaming ACN Protocol](E1.31)                                    | * [ESPixelStick](https://github.com/forkineye/ESPixelStick)<br>* [WLED](https://github.com/Aircoookie/WLED)                                                                                                                                                                                                                                                                                                   | :white_check_mark: | :x:                |
| [Thermaltake Riing Plus](Thermaltake-Riing)                              |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [FanBus](https://github.com/CalcProgrammer1/FanBus-Fan-Controller)       | * FanBus Fan Controller                                                                                                                                                                                                                                                                                                                                                                                       | :white_check_mark: | :x:                |
| Razer Chroma LED Controllers                                             | * Razer Chroma HDK<br>* Razer Chroma Addressable RGB Controller                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :warning:          |

* [ASUS ROG Aura Terminal](ASUS-Aura-Addressable-Header)
* [NZXT Hue+](NZXT-Hue-Plus)
* [NZXT Hue 2 Devices](NZXT-Hue-2)
  * NZXT Hue 2
  * NZXT Hue 2 Ambient
  * NZXT Smart Device V2
  * NZXT RGB & Fan Controller
* [Corsair Lighting Node Devices](Corsair-Lighting-Node-Devices)
  * Corsair Lighting Node Core
  * Corsair Lighting Node Pro
  * Corsair Commander Pro
  * Corsair LS100 Lighting Kit
  * Corsair 1000D Obsidian
  * Corsair SPEC OMEGA RGB
  * Corsair LT100
  * [Corsair Lighting Protocol (Arduino)](https://github.com/Legion2/CorsairLightingProtocol)
* [Keyboard Visualizer Arduino LED strips](Keyboard-Visualizer-LED-Strips)
* [E1.31 Streaming ACN Protocol](E1.31)
  * [ESPixelStick](https://github.com/forkineye/ESPixelStick)
  * [WLED](https://github.com/Aircoookie/WLED)
* [Thermaltake Riing Plus](Thermaltake-Riing)

## Lights

| Device                                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| ------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| Philips Hue                                                              | * Philips Hue Bridge V1<br>* Philips Hue Bridge V2<br>                                                                                                                                                                                                                                                                                                                                                        | :white_check_mark: | :warning:          |
| Philips Wiz                                                              |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :warning:          |
| Espurna (https://github.com/xoseperez/espurna)                           | * Ai-Thinker AiLight with Espurna                                                                                                                                                                                                                                                                                                                                                                             | :white_check_mark: | :x:                |
| [Yeelight](Yeelight)                                                     | * Yeelight Smart LED Bulb 1SE (Color)                                                                                                                                                                                                                                                                                                                                                                         | :white_check_mark: | :white_check_mark: |

## Fans and Coolers

| Device                                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| ------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| [AMD Wraith Prism](AMD-Wraith-Prism)                                     |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [Corsair Hydro Series](Corsair-Hydro-Series)                             | * Corsair H100i PRO RGB                                                                                                                                                                                                                                                                                                                                                                                       | :white_check_mark: | :white_check_mark: |
| [NZXT Kraken Xx2](NZXT-Kraken)                                           |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| [NZXT Kraken Xx3](NZXT-Hue-2)                                            |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :warning:          |
| [Thermaltake Riing Plus](Thermaltake-Riing)                              |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |

## Keyboards

| Device                                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                             | Direct Mode        | Effect Modes       |
| ------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| [ASUS ROG Aura Core Laptops](ASUS-Aura-Core)                             | * ASUS ROG Zephyrus M GM501GM                                                                                                                                                                                                                                                                                                                                                                                 | :warning:          | :white_check_mark: |
| [ASUS RGB Keyboards](ASUS-ROG-Strix-Flare)                               | * ASUS ROG Strix Flare                                                                                                                                                                                                                                                                                                                                                                                        | :white_check_mark: | :warning:          |
| [Corsair RGB Keyboards](Corsair-Peripheral-Protocol)                     | * Corsair K65 RGB<br>* Corsair K65 Lux RGB<br>* Corsair K65 RGB Rapidfire<br>* Corsair K68 RGB<br>* Corsair K70 RGB<br>* Corsair K70 Lux RGB<br>* Corsair K70 RGB Rapidfire<br>* Corsair K70 RGB MK.2<br>* Corsair K70 RGB MK.2 SE<br>* Corsair K70 RGB MK.2 Low Profile<br>* Corsair K95 RGB<br>* Corsair K95 RGB Platinum<br>* Corsair Strafe<br>* Corsair Strafe MK.2                                      | :white_check_mark: | :warning:          |
| [Ducky RGB Keyboards](Ducky-Keyboards)                                   | * Ducky Shine 7<br>* Ducky One 2<br>* Ducky One 2 TKL                                                                                                                                                                                                                                                                                                                                                         | :white_check_mark: | :warning:          |
| [EVision Keyboards](Redragon-K556-Devarajas)                             | * Redragon K530 Draconic<br>* Redragon K550 Yama<br>* Redragon K552 Kumara<br>* Redragon K556 Devarajas<br>* Tecware Phantom Elite<br>* Warrior Kane TC235                                                                                                                                                                                                                                                    | :x:                | :white_check_mark: |
| [HyperX RGB Keyboards](HyperX-Alloy-Elite)                               | * HyperX Alloy Elite<br>* HyperX Alloy Elite 2<br>* HyperX Alloy Origins                                                                                                                                                                                                                                                                                                                                      | :white_check_mark: | :warning:          |
| [Logitech RGB Keyboards](Logitech-Keyboards)                             | * Logitech G213<br>* Logitech G512<br>* Logitech G610<br>* Logitech G810 Orion Spectrum                                                                                                                                                                                                                                                                                                                       | :white_check_mark: | :white_check_mark: |
| [MSI Steelseries 3-Zone Keyboard](MSI-3-Zone-Keyboard)                   | * MSI GS63VR                                                                                                                                                                                                                                                                                                                                                                                                  | :white_check_mark: | :warning:          |
| [QMK Keyboards (OpenRGB Protocol)](QMK-OpenRGB-Protocol)                 | * Massdrop ALT<br>*Massdrop CTRL<br>* SonixQMK Keyboards (Redragon, Ajazz, etc)                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |
| Razer Chroma Keyboards                                                   | * Razer BlackWidow Chroma<br>* Razer BlackWidow Chroma Tournament Edition<br>* Razer BlackWidow Chroma V2<br>* Razer Cynosa Chroma<br>* Razer Huntsman<br>* Razer Huntsman Elite<br>* Razer Ornata Chroma                                                                                                                                                                                                     | :white_check_mark: | :warning:          |
| SteelSeries Keyboards                                                    | * SteelSeries Apex 5<br>* SteelSeries Apex 7<br>* SteelSeries Apex 7 TKL<br>* SteelSeries Apex Pro<br>* SteelSeries Apex Pro TKL                                                                                                                                                                                                                                                                              |                    |                    |
| [TTEsports Poseidon Z RGB](Thermaltake-Poseidon-Z-RGB)                   |                                                                                                                                                                                                                                                                                                                                                                                                               | :white_check_mark: | :white_check_mark: |

* [ASUS ROG Aura Core Laptops](ASUS-Aura-Core)
  * ASUS ROG Zephyrus M GM501GM
* [Corsair RGB Keyboards](Corsair-Peripheral-Protocol)
  * Corsair K65 RGB
  * Corsair K65 Lux RGB
  * Corsair K65 RGB Rapidfire
  * Corsair K68 RGB
  * Corsair K70 RGB
  * Corsair K70 Lux RGB
  * Corsair K70 RGB Rapidfire
  * Corsair K70 RGB MK.2
  * Corsair K70 RGB MK.2 SE
  * Corsair K70 RGB MK.2 Low Profile
  * Corsair K95 RGB
  * Corsair K95 RGB Platinum
  * Corsair Strafe
  * Corsair Strafe MK.2
* [Ducky RGB Keyboards](Ducky-Keyboards)
  * Ducky Shine 7
  * Ducky One 2
  * Ducky One 2 TKL
* [HyperX RGB Keyboards](HyperX-Alloy-Elite)
  * HyperX Alloy Elite
  * HyperX Alloy Origins
* [Logitech RGB Keyboards](Logitech-Keyboards)
  * Logitech G213
  * Logitech G512
  * Logitech G610
  * Logitech G810 Orion Spectrum
* [MSI Steelseries 3-Zone Keyboard](MSI-3-Zone-Keyboard)
  * MSI GS63VR
* [Redragon Keyboards (and compatibles)](Redragon-K556-Devarajas)
  * Redragon K550 Yama
  * Redragon K552 Kumara
  * Redragon K556 Devarajas
  * Tecware Phantom Elite
  * Warrior Kane TC235
* SteelSeries Keyboards
  * SteelSeries Apex 5
  * SteelSeries Apex 7
  * SteelSeries Apex 7 TKL
  * SteelSeries Apex Pro
  * SteelSeries Apex Pro TKL
* [TTEsports Poseidon Z RGB](Thermaltake-Poseidon-Z-RGB)

## Mice

| Device                                                                   | Verified Products                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Direct Mode        | Effect Modes       |
| ------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ |
| ASUS Mice                                                                | * ROG Gladius II<br>* ROG Gladius II Core<br>* ROG Gladius II Origin                                                                                                                                                                                                                                                                                                                                                                                                                     | :white_check_mark: | :white_check_mark: |
| [Corsair Mice](Corsair-Peripheral-Protocol)                              | * Corsair M65 RGB PRO<br>* Corsair M65 RGB Elite                                                                                                                                                                                                                                                                                                                                                                                                                                         | :white_check_mark: | :warning:          |
| Glorious Model O                                                         |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | :warning:          | :white_check_mark: |
| HyperX Mice                                                              | * HyperX Pulsefire FPS Pro<br>* HyperX Pulsefire Surge                                                                                                                                                                                                                                                                                                                                                                                                                                   | :white_check_mark: | :warning:          |
| [Logitech Mice](Logitech-G203)                                           | * Logitech G203 Prodigy<br>* Logitech G203 Lightsync<br>* Logitech G403 Prodigy<br>* Logitech G403 Hero<br>* Logitech G502 Proteus Spectrum<br>* Logitech G502 Hero<br>* Logitech G Lightspeed Wireless Gaming Mouse<br>* Logitech G Pro Wireless Gaming Mouse (Wired)<br>* Logitech G Powerplay Mousepad with Lightspeed                                                                                                                                                                | :warning:          | :white_check_mark: |
| Razer Chroma Mice                                                        | * Razer DeathAdder Chroma<br>* Razer Diamondback<br>* Razer Mamba Elite<br>* Razer Mamba Tournament Edition                                                                                                                                                                                                                                                                                                                                                                              | :white_check_mark: | :warning:          |
| [Redragon Mice](Redragon-M711-Cobra)                                     | * Redragon M711 Cobra<br>* Redragon M715 Dagger                                                                                                                                                                                                                                                                                                                                                                                                                                          | :x:                | :white_check_mark: |
| Roccat Mice                                                              | * Roccat Kone Aimo                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | :white_check_mark: |                    |
| SteelSeries Mice                                                         | * SteelSeries Rival 100<br>* SteelSeries Rival 100 DotA 2 Edition<br>* SteelSeries Rival 105<br>* SteelSeries Rival 110<br>* SteelSeries Rival 300<br>* Acer Predator Gaming Mouse (Rival 300)<br>* SteelSeries Rival 300 CS:GO Fade Edition<br>* SteelSeries Rival 300 CS:GO Fade Edition (stm32)<br>* SteelSeries Rival 300 CS:GO Hyperbeast Edition<br>* SteelSeries Rival 300 DotA 2 Edition<br>* SteelSeries Rival 300 HP Omen Edition<br>* SteelSeries Rival 300 Black Ops Edition |                    |                    |

* ASUS Mice
  * ROG Gladius II
  * ROG Gladius II Core
  * ROG Gladius II Origin
* [Corsair Mice](Corsair-Peripheral-Protocol)
  * Corsair M65 RGB PRO
  * Corsair M65 RGB Elite
* Glorious Model O
* HyperX Mice
  * HyperX Pulsefire Surge
* [Logitech Mice](Logitech-G203)
  * Logitech G203 Prodigy
  * Logitech G203 Lightsync
  * Logitech G403 Prodigy
  * Logitech G403 Hero
  * Logitech G502 Proteus Spectrum
  * Logitech G502 Hero
  * Logitech G Lightspeed Wireless Gaming Mouse
  * Logitech G Pro Wireless Gaming Mouse (Wired)
  * Logitech G Powerplay Mousepad with Lightspeed"
* [Redragon Mice](Redragon-M711-Cobra)
  * Redragon M711 Cobra
  * Redragon M715 Dagger
* Roccat Mice
  * Roccat Kone Aimo
* SteelSeries Mice
  * SteelSeries Rival 100
  * SteelSeries Rival 100 DotA 2 Edition
  * SteelSeries Rival 105
  * SteelSeries Rival 110
  * SteelSeries Rival 300
  * Acer Predator Gaming Mouse (Rival 300)
  * SteelSeries Rival 300 CS:GO Fade Edition
  * SteelSeries Rival 300 CS:GO Fade Edition (stm32)
  * SteelSeries Rival 300 CS:GO Hyperbeast Edition
  * SteelSeries Rival 300 DotA 2 Edition
  * SteelSeries Rival 300 HP Omen Edition
  * SteelSeries Rival 300 Black Ops Edition


## Mousemats

* [Cooler Master MP750](Cooler-Master-MP750)
* Corsair MM800 Polaris
* HyperX Fury Ultra

## Game Controlers

* [Sony DualShock 4](Sony-DualShock-4)

## Other

* Corsair ST100 Headset Stand

## Other projects integrated

* [OpenRazer](https://github.com/openrazer/openrazer) / [OpenRazer-Win32](https://github.com/CalcProgrammer1/openrazer-win32)
  * Keyboards
    * Razer BlackWidow 2019
    * Razer BlackWidow Chroma
    * Razer BlackWidow Chroma Overwatch
    * Razer BlackWidow Chroma Tournament Edition
    * Razer BlackWidow Chroma V2
    * Razer BlackWidow Elite
    * Razer BlackWidow X Chroma
    * Razer BlackWidow X Tournament Edition Chroma
    * Razer Cynosa Chroma
    * Razer DeathStalker Chroma
    * Razer Ornata Chroma
    * Razer Huntsman
    * Razer Huntsman Elite
    * Razer Huntsman Tournament Edition
  * Mice
    * Razer Abyssus Elite DVa Edition
    * Razer Abyssus Essential
    * Razer Basilisk
    * Razer DeathAdder Chroma
    * Razer DeathAdder Elite
    * Razer Diamondback Chroma
    * Razer Lancehead Tournament Edition
    * Razer Mamba 2012
    * Razer Mamba Chroma
    * Razer Mamba Elite
    * Razer Mamba Tournament Edition
    * Razer Naga Chroma
    * Razer Naga Epic Chroma (*)
    * Razer Naga Hex V2
    * Razer Naga Trinity
    * Razer Viper Ultimate
  * Laptops
    * Razer Blade Stealth
    * Razer Blade Stealth (Late 2016)
    * Razer Blade Stealth (Mid 2017)
    * Razer Blade Stealth (Late 2017)
    * Razer Blade Stealth (2019)
    * Razer Blade Stealth (Late 2019)
    * Razer Blade (Late 2016)
    * Razer Blade (QHD)
    * Razer Blade 15 (2018)
    * Razer Blade 15 (2018) Mercury
    * Razer Blade 15 (2018) Base Model
    * Razer Blade 15 (2019) Advanced
    * Razer Blade 15 (Mid 2019) Mercury
    * Razer Blade 15 (Mid 2019) Base Model
    * Razer Blade 15 Studio Edition (2019)
    * Razer Blade Pro (Late 2016)
    * Razer Blade Pro (2017)
    * Razer Blade Pro FullHD (2017)
    * Razer Blade Pro 17 (2019)
    * Razer Blade Pro (Late 2019)
  * Headsets
    * Razer Kraken 7.1 Chroma
    * Razer Kraken V2 Chroma
    * Razer Tiamat 7.1 V2 (*)
  * Mousemats
    * Razer Firefly
    * Razer Goliathus Chroma
    * Razer Goliathus Extended Chroma
  * Speakers
    * Razer Nommo Chroma
    * Razer Nommo Pro
  * Accessories
    * Razer Base Station Chroma
    * Razer Chroma HDK
    * Razer Core
    * Razer Mug Holder Chroma

(*) - Device not supported in upstream OpenRazer and requires a custom build.

* Faustus (ASUS TUF Laptop Keyboards) (Linux)

## **Disabled Devices**

The following devices have support in OpenRGB but this support is currently disabled due to unimplemented detection or bricking risk.

### Disabled Motherboard RGB Systems

* [MSI Mystic Light](MSI-Mystic-Light)

### Disabled RGB RAM Modules

* [Gigabyte Aorus RGB RAM (Partial support)](Gigabyte-RGB-Fusion-2.0-DRAM)
