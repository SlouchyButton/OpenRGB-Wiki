# ![20200101_181654](uploads/e7d3b08c861d83bac5304b945d59c6c4/20200101_181654.jpg)

The Patriot Viper RGB memory enumerates an SMBus device at 0x77.  It uses an SMBus protocol that uses two SMBus byte writes for writing 3 bytes to an address.  The protocol works as follows:

Write \<Value 0\> to \<Address\>

Write \<Value 1\> to \<Value 2\>

To initiate a write sequence, you have to write 0xFF to address 0xFF twice.

## **Addresses**

| Address | Value 0   | Value 1    | Value 2     | Description     |
| ------- | --------- | ---------- | ----------- | --------------- |
| 0x01    | 0x04      | 0x00       | 0x00        | Start (?)       |
| 0x03    | Mode      | Step       | Speed       | Mode            |
| 0x30    | LED 0 Red | LED 0 Blue | LED 0 Green |                 |
| 0x31    | LED 1 Red | LED 1 Blue | LED 1 Green |                 |
| 0x32    | LED 2 Red | LED 2 Blue | LED 2 Green |                 |
| 0x33    | LED 3 Red | LED 3 Blue | LED 3 Green |                 |
| 0x34    | LED 4 Red | LED 4 Blue | LED 4 Green |                 |
| 0x35    | 0x01      | 0x00       | 0x00        | Apply Changes   |
| 0x3B    | LED 0 Red | LED 0 Blue | LED 0 Green | Effect Color    |
| 0x3C    | LED 1 Red | LED 1 Blue | LED 1 Green | Effect Color    |
| 0x3D    | LED 2 Red | LED 2 Blue | LED 2 Green | Effect Color    |
| 0x3E    | LED 3 Red | LED 3 Blue | LED 3 Green | Effect Color    |
| 0x3F    | LED 4 Red | LED 4 Blue | LED 4 Green | Effect Color    |
| 0xFF    | 0xFF      | 0xFF       | 0xFF        | Start Frame (?) |

## **Modes**

| Mode Value | Mode Description | Slow Speed | Default Speed | Fast Speed | Steps | Sub-steps |
| ---------- | ---------------- | ---------- | ------------- | ---------- | ----- | --------- |
| 0x00       | Dark             |            |               |            |       |           |
| 0x01       | Breathing        |            | 0x0C          |            | 4     | 1         |
| 0x02       | Viper            | 0xC8       | 0x3C          | 0x14       | 4     | 6         |
| 0x03       | Heartbeat        |            | 0x64          |            | 6     | 59        |
| 0x04       | Marquee          |            | 0x64          |            | 3     | 30        |
| 0x05       | Raindrop         |            | 0x64          |            |       |           |
| 0x06       | Aurora           |            | 0x64          |            | 4     | 0         |
| 0x07       | Direct           |            | 0x64          |            |       |           |
| 0x08       | Neon             |            | 0x64          |            | 0     | 5         |
| 0xAA       | Apply Effect     | N/A        | N/A           | N/A        |       |           |
| 0xFA       | Start Effect     | N/A        | N/A           | N/A        |       |           |


Modes are applied by sending Mode Value to Address 0x03 followed by sending Value 1 (step) to Value 2 (speed). Then you send 0xAA (Apply Effect) to Address 0x03 followed by sending 0x00 to Sub Step.

Each mode has specific timing for steps change making it too slow will make the flow of animation weird, making it too quick will make LEDs glitchy.

From my testing current settings are on the sweet spot with:
**Viper**: After sub-step 0 wait 3s, after sub-step 4 wait 1s, after sub-step 5 wait 0.59s and at the end of the sub-step loop (sub-step 6) wait 7 seconds (this can be variable together with sub-step 0)
**Neon**: Wait 600ms between sub-steps
**Heartbeat**: Wait 100ms between sub-steps
**Marquee**: Wait 300ms between sub-steps
**Aurora**: Wait 300ms between steps
**Breathing**: Wait 3s between steps and sub-steps

Variable speed is not implemented yet, because it will require changing both speed of the animation (Value 2 being sent to i2c bus) *and* delays.

Raindrop, Direct and Dark modes don't require steps to be kept in sync. (Raindrop is random effect which flashes individual RAM sticks - keeping them in sync is not needed)

Direct mode together with setting individual colors with registers 0x3B-0x3F is preffered because using 0x30-0x34 directly will A) make LEDs less bright B) LEDs will flash dark between color changes - extremely annoying (unusable) with Effects.
