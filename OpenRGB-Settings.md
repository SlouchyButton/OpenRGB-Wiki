# OpenRGB Setting

OpenRGB's configuration file is called OpenRGB.json and exists in the OpenRGB configuration directory.  The configuration directory depends on your OS:

* Windows: %APPDATA%\OpenRGB\

* Linux: ~/.config/OpenRGB/

The configuration directory also contains profile files (.orp) and saved size configuration files (.ors).

## OpenRGB JSON Configuration

The OpenRGB.json file contains several top-level JSON keys.  The keys are listed below along with the settings they contain.

### `Detectors` - Enable/Disable Devices

* `detectors` - List of booleans.  Values are `true` or `false`.
  * To disable a specific detector find it in the list and replace `true` with `false`.  To re-enable you can do the reverse.
* `hid_safe_mode` - Boolean.
  * When set to `true`, this setting uses a slightly slower detection scheme for USB HID devices that works around a rare condition where the HID detection can crash the application.

### `UserInterface` - Change User Interface Settings
* `minimize_on_close` - Boolean.  Value is `true` or `false`
  * When `true`, closing the application with the close "X" button hides the app to the tray.  When `false`, closing the application with the close button exits the app.
* `geometry` - Settings for window geometry
  * `x` - Integer, X-position of the app window
  * `y` - Integer, Y-position of the app window
  * `width` - Integer, Width of the app window
  * `height` - Integer, Height of the app window
  * `save_on_exit` - Boolean, when `true` the geometry values are updated with the window's size and position upon exiting the app.
* `numerical_labels` - Boolean.  Value is `true` or `false`
  * When `true`, LEDs without key labels in the LED view will be labeled with their index in the device.  When `false`, the LEDs will not be labeled.

### `Server` - Change SDK Server Settings
* `all_controllers` - Boolean.  Value is `true` or `false`
  * When `true`, devices from connected SDK Clients will be available on this instance's SDK Server.  When `false`, only local devices controlled by this instance will be available on the server.

### `DebugDevices` - Configure Debug Devices

* `devices` - List of device configurations containing the following keys:
  * `type` - String, one of "keyboard", "dram", "gpu", "motherboard", or "argb"

The types are defined as follows

1. `keyboard`: (Matrix zone and a linear zone)
2. `dram`: (Linear zone and single zone)
3. `gpu`: (Linear zone and single zone)
4. `motherboard`: (Linear zone and single zone)
5. `argb`: (Resizable linear zone)

See below example:

```json
"DebugDevices": {
    "devices": [
        {
            "type": "dram"
        }
    ]
}
```

#### **Custom debug devices**

Under the `DebugDevices` create a list object labeled `CustomDevices`

In each entry in the `CustomDevices` list you will need the following:

* A Device name (`DeviceName`. A string)

* A Device description (`DeviceDescription`. A String)

* A Device location (`DeviceLocation`. A String)

* A Device Serial (`DeviceSerial`. A String)

* A Device Type (`DeviceType`. A String)

   1. "motherboard"

   2. "dram"

   3. "gpu"

   4. "cooler"

   5. "led_strip"

   6. "keyboard"

   7. "mouse"

   8. "mousemat"

   9. "headset"

   10. "headset_stand"

   11. "gamepad"

   12. "light"

   13. "speaker"

   14. "unknown"

* A list of zones (`DeviceZones`. A list)

  Each entry must contain:

  * A Name (`name`. A String)

  * A Type (`type`. A String)

    1. "single"

    2. "linear"

    3. "matrix"

       * A matrix height (`matrix_height`, An Int)

       * A matrix width (`matrix_width`, An Int)

       * A matrix map (`matrix_map`, A List of Lists of Ints)

  * A Minimum LED Count (`leds_min`. An Int)

  * A Maximum LED Count (`leds_max`. An Int)

  * A starting LED Count (`leds_count`. An Int)

  * A list of LED names (This is optional. `custom_labels`. A list of strings)

The final structure ends up looking like this

```json
    "DebugDevices": {
        "CustomDevices": [
            {
               "DeviceName": "Custom Test Devce 1",
                "DeviceDescription": "A device to test custom debug devices",
                "DeviceLocation": "Debug Test Device Location",
                "DeviceSerial": "hehehehehe",
                "DeviceType": "keyboard",
                "DeviceVersion": "1",
                "DeviceZones": [
                    {
                        "custom_labels": [
                            "hehe",
                            "no"
                        ],
                        "leds_count": 2,
                        "leds_max": 2,
                        "leds_min": 2,
                        "matrix_height": 2,
                        "matrix_map": [
                            [0],
                            [1]
                        ],
                        "matrix_width": 1,
                        "name": "TestZone",
                        "type": "matrix"
                    }
                ]
            }
        ]
    },
```

### `Theme` (Windows only)

* `theme` - String, one of "auto", "light", or "dark"

Example:

```json
"Theme": {
    "theme": "dark"
}
```

### Minimizing on X (Close)

Stored under the ``Minimize`` key

The Subkey is ``minimize_on_close``

``true`` will make it minimize when you hit X

``false`` will completely close OpenRGB

The structure is;

```json
  "Minimize": {
    "minimize_on_close": true
}
```

As a side note; You can Re-show OpenRGB by double clicking the tray icon

### `E131Devices` - E1.31 Device Configuration

* `devices` - List of device definitions.  See [E1.31 wiki page](E1.31) for details.

### `LEDStripDevices` - Serial LED Strip Controller Configuration

* `devices` - List of device definitions.  See [LED Strip wiki page](Keyboard-Visualizer-LED-Strips) for details.

### `EspurnaDevices` - Espurna Device Configuration

* `devices` - List of device definitions.  See [Espurna wiki page](Espurna) for details.

### `QMKOpenRGBDevices` - QMK OpenRGB Devices
* `devices` - List of device definitions.  See [QMK OpenRGB Protocol wiki page](QMK-OpenRGB-Protocol) for details.

### `PhilipsHueDevices` - Philips Hue Device Configuration
* `bridges` - List of Philips Hue bridges.  See [Philips Hue wiki page](Philips-Hue) for details.

### `PhilipsWizDevices` - Philips Wiz Device Configuration
* `devices` - List of device definitions.  See [Philips Wiz wiki page](Philips-Wiz) for details.

### `YeelightDevices` - Yeelight Device Configuration
* `devices` - List of device definitions.  See [Yeelight wiki page](Yeelight) for details.

